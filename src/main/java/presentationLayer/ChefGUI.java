/*
 * Created by JFormDesigner on Mon May 20 19:56:07 EEST 2019
 */

package presentationLayer;

import businessLayer.IRestaurantProcessing;
import businessLayer.Restaurant;

import java.awt.*;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.*;
import javax.swing.GroupLayout;

/**
 * @author Irina Marginean
 */
public class ChefGUI extends JFrame implements Observer {
    IRestaurantProcessing restaurant;
    public ChefGUI(IRestaurantProcessing restaurant) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch(Exception e) {
            System.out.println("Error");
        }
        this.restaurant = restaurant;
        initComponents();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Irina Marginean
        scrollPane1 = new JScrollPane();
        orders = new JTextArea();
        label1 = new JLabel();
        goToAdmin = new JButton();

        //======== this ========
        setTitle("Chef");
        Container contentPane = getContentPane();

        //======== scrollPane1 ========
        {
            scrollPane1.setViewportView(orders);
        }

        //---- label1 ----
        label1.setText("Active orders");

        //---- goToAdmin ----
        goToAdmin.setText("Go to Admin");

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addComponent(label1, GroupLayout.PREFERRED_SIZE, 284, GroupLayout.PREFERRED_SIZE)
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 457, GroupLayout.PREFERRED_SIZE)
                            .addGap(53, 53, 53)
                            .addComponent(goToAdmin, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)))
                    .addContainerGap(17, Short.MAX_VALUE))
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                        .addComponent(goToAdmin, GroupLayout.PREFERRED_SIZE, 91, GroupLayout.PREFERRED_SIZE)
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addComponent(label1, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)))
                    .addContainerGap(39, Short.MAX_VALUE))
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Irina Marginean
    private JScrollPane scrollPane1;
    private JTextArea orders;
    private JLabel label1;
    private JButton goToAdmin;
    // JFormDesigner - End of variables declaration  //GEN-END:variables

    protected void addGoToAdminButtonListener(final ActionListener actionListener) {
        goToAdmin.addActionListener(actionListener);
    }

    protected void setOrders(String text) {
        orders.setText(text);
    }

    public void showWarningMessage(String message) {
        JOptionPane.showMessageDialog(getContentPane(), message);
    }

    @Override
    public void update(Observable o, Object arg) {
        showWarningMessage("Chef observed new order");
    }
}
