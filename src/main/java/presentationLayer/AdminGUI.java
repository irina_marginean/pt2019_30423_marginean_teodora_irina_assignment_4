/*
 * Created by JFormDesigner on Mon May 20 19:55:31 EEST 2019
 */

package presentationLayer;

import businessLayer.IRestaurantProcessing;
import businessLayer.MenuItem;
import businessLayer.TableBLL;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.GroupLayout;

/**
 * @author Irina Marginean
 */
public class AdminGUI extends JFrame {
    private IRestaurantProcessing restaurant;
    public AdminGUI(IRestaurantProcessing restaurant, JTable table, JComboBox comboBox) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch(Exception e) {
            System.out.println("Error");
        }
        this.restaurant = restaurant;
        initComponents(restaurant, table, comboBox);
    }

    private void initComponents(IRestaurantProcessing restaurant, JTable table1, JComboBox comboBox) {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Irina Marginean
        scrollPane1 = new JScrollPane();
        table = table1;
        panel1 = new JPanel();
        this.comboBox = comboBox;
        nameField = new JTextField();
        priceField = new JTextField();
        addButton = new JButton();
        editButton = new JButton();
        deleteButton = new JButton();
        label1 = new JLabel();
        label2 = new JLabel();
        label3 = new JLabel();
        goToWaiterButton = new JButton();
        scrollPane2 = new JScrollPane();
        textArea1 = new JTextArea();
        addCompositeButton = new JButton();
        createCompositeButton = new JButton();
        label4 = new JLabel();

        //======== this ========
        setTitle("Admin");
        Container contentPane = getContentPane();

        //======== scrollPane1 ========
        {
            scrollPane1.setViewportView(table);
        }

        //======== panel1 ========
        {

            // JFormDesigner evaluation mark
            panel1.setBorder(new javax.swing.border.CompoundBorder(
                new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
                    "JFormDesigner Evaluation", javax.swing.border.TitledBorder.CENTER,
                    javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
                    java.awt.Color.red), panel1.getBorder())); panel1.addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});


            //---- addButton ----
            addButton.setText("Add");

            //---- editButton ----
            editButton.setText("Edit");

            //---- deleteButton ----
            deleteButton.setText("Delete");

            //---- label1 ----
            label1.setText("Select an item to Edit/Delete/Add to composite item");

            //---- label2 ----
            label2.setText("Name");

            //---- label3 ----
            label3.setText("Price");

            //---- goToWaiterButton ----
            goToWaiterButton.setText("Go to Waiter ");

            //======== scrollPane2 ========
            {
                scrollPane2.setViewportView(textArea1);
            }

            //---- addCompositeButton ----
            addCompositeButton.setText("Add to Composite Item");

            //---- createCompositeButton ----
            createCompositeButton.setText("Create Composite Item");

            //---- label4 ----
            label4.setText("Composite Item");

            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(
                panel1Layout.createParallelGroup()
                    .addComponent(comboBox, GroupLayout.Alignment.TRAILING)
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panel1Layout.createParallelGroup()
                            .addComponent(nameField)
                            .addGroup(GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                                .addGroup(panel1Layout.createParallelGroup()
                                    .addComponent(label2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(addCompositeButton, GroupLayout.DEFAULT_SIZE, 254, Short.MAX_VALUE)
                                    .addGroup(panel1Layout.createSequentialGroup()
                                        .addGap(6, 6, 6)
                                        .addComponent(label3))
                                    .addComponent(createCompositeButton, GroupLayout.PREFERRED_SIZE, 212, GroupLayout.PREFERRED_SIZE)
                                    .addComponent(scrollPane2))
                                .addGroup(panel1Layout.createParallelGroup()
                                    .addGroup(panel1Layout.createSequentialGroup()
                                        .addGap(30, 30, 30)
                                        .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                            .addComponent(addButton, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 96, Short.MAX_VALUE)
                                            .addComponent(editButton, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 96, Short.MAX_VALUE)
                                            .addComponent(deleteButton, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 96, Short.MAX_VALUE))
                                        .addGap(16, 16, 16))
                                    .addGroup(GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                                        .addComponent(goToWaiterButton, GroupLayout.PREFERRED_SIZE, 134, GroupLayout.PREFERRED_SIZE)
                                        .addGap(8, 8, 8))))
                            .addComponent(priceField, GroupLayout.Alignment.TRAILING)
                            .addGroup(panel1Layout.createSequentialGroup()
                                .addGroup(panel1Layout.createParallelGroup()
                                    .addComponent(label4)
                                    .addComponent(label1, GroupLayout.PREFERRED_SIZE, 338, GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 58, Short.MAX_VALUE)))
                        .addContainerGap())
            );
            panel1Layout.setVerticalGroup(
                panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(label1)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(comboBox, GroupLayout.PREFERRED_SIZE, 47, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(label2)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(nameField, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(label3)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(priceField, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                        .addGroup(panel1Layout.createParallelGroup()
                            .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(30, 30, 30)
                                .addComponent(addButton)
                                .addGap(18, 18, 18)
                                .addComponent(editButton)
                                .addGap(18, 18, 18)
                                .addComponent(deleteButton))
                            .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(label4)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scrollPane2, GroupLayout.PREFERRED_SIZE, 111, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(addCompositeButton)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                    .addComponent(createCompositeButton)
                                    .addComponent(goToWaiterButton, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE))))
                        .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            );
        }

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 365, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(12, 12, 12))
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                        .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 448, GroupLayout.PREFERRED_SIZE)
                        .addComponent(panel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addContainerGap())
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Irina Marginean
    private JScrollPane scrollPane1;
    private JTable table;
    private JPanel panel1;
    private JComboBox comboBox;
    private JTextField nameField;
    private JTextField priceField;
    private JButton addButton;
    private JButton editButton;
    private JButton deleteButton;
    private JLabel label1;
    private JLabel label2;
    private JLabel label3;
    private JButton goToWaiterButton;
    private JScrollPane scrollPane2;
    private JTextArea textArea1;
    private JButton addCompositeButton;
    private JButton createCompositeButton;
    private JLabel label4;
    // JFormDesigner - End of variables declaration  //GEN-END:variables

    protected void addGoToWaiterButtonListener(final ActionListener actionListener) {
        goToWaiterButton.addActionListener(actionListener);
    }

    protected void addAddButtonListener(final ActionListener actionListener) {
        addButton.addActionListener(actionListener);
    }

    protected void addEditButtonListener(final ActionListener actionListener) {
        editButton.addActionListener(actionListener);
    }

    protected void addDeleteButtonListener(final ActionListener actionListener) {
        deleteButton.addActionListener(actionListener);
    }

    protected void addAddCompositeButtonListener(final ActionListener actionListener) {
        addCompositeButton.addActionListener(actionListener);
    }

    protected void addCreateCompositeButtonListener(final ActionListener actionListener) {
        createCompositeButton.addActionListener(actionListener);
    }


    public void showWarningMessage(String message) {
        JOptionPane.showMessageDialog(getContentPane(), message);
    }

    public String getName() { return nameField.getText(); }
    public String getPrice() { return priceField.getText(); }

    protected MenuItem getSelectedItem(ActionEvent evt) {
        if (comboBox.getSelectedItem() != null) {
            return (MenuItem) comboBox.getSelectedItem();
        }
        return null;
    }

    public void setTextArea1(String text){
        textArea1.setText(text);
    }

    public void setPriceField(double price)
    {
        priceField.setText(price + "");
    }

}
