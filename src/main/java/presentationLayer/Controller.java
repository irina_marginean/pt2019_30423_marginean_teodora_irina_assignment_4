package presentationLayer;

import businessLayer.*;
import dataLayer.RestaurantSerializator;

import javax.swing.*;
import java.util.HashSet;
import java.util.Set;

public class Controller {

    private AdminGUI adminGUI;
    private WaiterGUI waiterGUI;
    private ChefGUI chefGUI;
    private Restaurant restaurant;
    private RestaurantSerializator restaurantSerializator;
    private CompositeProduct compositeProduct;
    private Set<MenuItem> orderItems;

    public Controller()
    {
        restaurantSerializator = new RestaurantSerializator();
        restaurant = new Restaurant();
        restaurant.setMenu(restaurantSerializator.DeserializeMenu());
        chefGUI = new ChefGUI(restaurant);
        restaurant.addObserver(chefGUI);
        startAdmin();
    }

    private void startAdmin()
    {
        JTable table = updateAdminTable();
        JComboBox comboBox = updateComboBox();
        adminGUI = new AdminGUI(restaurant, table, comboBox);
        initializeAdminButtonListeners();
        compositeProduct = new CompositeProduct();
        adminGUI.setVisible(true);
    }

    private void startWaiter()
    {
        JTable table = updateWaiterTable();
        JComboBox comboBox = updateComboBox();
        waiterGUI = new WaiterGUI(restaurant, table, comboBox);
        initializeWaiterButtonListeners();
        orderItems =  new HashSet<>();
        waiterGUI.setVisible(true);
    }

    private void startChef()
    {
        chefGUI = new ChefGUI(restaurant);
        StringBuilder stringBuilder = new StringBuilder();
        for (Order order : restaurant.getOrders().keySet())
        {
            stringBuilder.append("Order number: " + order.orderID + "\ntable: " + order.table + "\n");
            for (MenuItem menuItem : restaurant.getOrders().get(order))
            {
                stringBuilder.append("\t" + menuItem.getName());
            }
            stringBuilder.append("\n");
        }
        chefGUI.setOrders(stringBuilder.toString());
        initializeChefButtonListeners();
        chefGUI.setVisible(true);
    }

    private JTable updateAdminTable()
    {
        TableBLL<MenuItem> tableBLL = new TableBLL<>();
        JTable table = tableBLL.createTable(restaurant.getMenu(), MenuItem.class);
        return table;
    }

    private JTable updateWaiterTable()
    {
        TableBLL<Order> tableBLL = new TableBLL<>();
        JTable table = tableBLL.createTable(restaurant.getAllOrders(), Order.class);
        return table;
    }

    private JComboBox updateComboBox()
    {
        JComboBox comboBox = new JComboBox(restaurant.getMenu().toArray());
        return  comboBox;
    }

    private void initializeAdminButtonListeners() {
        adminGUI.addAddButtonListener(e ->{
            try {
                MenuItem menuItem = new BaseProduct();
                menuItem.setName(adminGUI.getName());
                menuItem.setPrice(Double.parseDouble(adminGUI.getPrice()));
                restaurant.createMenuItem(menuItem);
                restaurantSerializator.SerializeMenu(restaurant.getMenu());
                adminGUI.dispose();
                startAdmin();
            }
            catch (NumberFormatException ex)
            {
                adminGUI.showWarningMessage("The input is not correct");
            }
        });
        adminGUI.addEditButtonListener(e -> {
            try
            {
                MenuItem selectedItem = adminGUI.getSelectedItem(e);
                String name = adminGUI.getName();
                double price = Double.parseDouble(adminGUI.getPrice());
                restaurant.editMenuItem(selectedItem, name, price);
                restaurantSerializator.SerializeMenu(restaurant.getMenu());
                adminGUI.dispose();
                startAdmin();
            }
            catch (NumberFormatException ex)
            {
                adminGUI.showWarningMessage("Input is not correct");
            }

        });
        adminGUI.addDeleteButtonListener(e -> {
            MenuItem selectedItem = adminGUI.getSelectedItem(e);
            restaurant.deleteMenuItem(selectedItem);
            restaurantSerializator.SerializeMenu(restaurant.getMenu());
            adminGUI.dispose();
            startAdmin();
        });
        adminGUI.addGoToWaiterButtonListener(e -> {
            adminGUI.dispose();
            startWaiter();
        });
        adminGUI.addAddCompositeButtonListener(e -> {
            MenuItem selectedItem = adminGUI.getSelectedItem(e);
            compositeProduct.addMenuItem(selectedItem);
            adminGUI.setTextArea1(compositeProduct.getItemsString());
            adminGUI.setPriceField(compositeProduct.computePrice());
        });
        adminGUI.addCreateCompositeButtonListener(e -> {
            compositeProduct.setName(adminGUI.getName());
            compositeProduct.setPrice(compositeProduct.computePrice());
            restaurant.createMenuItem((MenuItem) compositeProduct);
            restaurantSerializator.SerializeMenu(restaurant.getMenu());
            adminGUI.dispose();
            startAdmin();
        });
    }

    private void initializeWaiterButtonListeners() {
        waiterGUI.addAddToOrderButtonListener(e -> {
            MenuItem selectedItem = waiterGUI.getSelectedItem(e);
            orderItems.add(selectedItem);
            StringBuilder stringBuilder = new StringBuilder();
            for (MenuItem menuItem : orderItems)
            {
                stringBuilder.append(menuItem.getName() + "\n");
            }
            waiterGUI.setOrdersField(stringBuilder.toString());
        });
        waiterGUI.addPlaceOrderButtonListener(e -> {
            try{
                Order order = restaurant.createOrder(Integer.parseInt(waiterGUI.getTable()), orderItems);
                restaurant.generateBill(order);
                waiterGUI.dispose();
                startWaiter();
            }
            catch (NumberFormatException ex)
            {
                waiterGUI.showWarningMessage("Could not find table");
            }
        });
        waiterGUI.addGoToChefButtonListener(e -> {
            waiterGUI.dispose();
            startChef();
        });
    }

    private void initializeChefButtonListeners() {
        chefGUI.addGoToAdminButtonListener(e -> {
            chefGUI.dispose();
            startAdmin();
        });
    }
}
