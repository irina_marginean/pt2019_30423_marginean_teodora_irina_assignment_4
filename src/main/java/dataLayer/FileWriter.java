package dataLayer;

import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.Restaurant;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class FileWriter {

    private static final DateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

    public void writeBill(Restaurant restaurant, Order order)
    {
        try {
            PrintWriter writer = new PrintWriter("order_" + order.orderID + ".txt", "UTF-8");
            writer.println("Order number: " + order.orderID);
            writer.println("Date: " + simpleDateFormat.format(order.date));
            writer.println("Table: " + order.table);
            writer.println("");
            for (MenuItem menuItem : restaurant.getOrders().get(order)) {
                writer.println("Menu item: " + menuItem.getName());
                writer.println("\t\t" + String.format("%.2f", menuItem.getPrice()) + " RON");
            }
            writer.println("");
            writer.println("Total price: " + String.format("%.2f", restaurant.computeOrderPrice(order)) + " RON");
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

}
