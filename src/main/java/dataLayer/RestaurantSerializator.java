package dataLayer;

import businessLayer.MenuItem;

import java.io.*;
import java.util.HashSet;
import java.util.Set;

public class RestaurantSerializator {
    public void SerializeMenu(Set<MenuItem> menu)
    {
        try
        {
            FileOutputStream fileOutputStream = new FileOutputStream("menu.ser");
            ObjectOutputStream objectOutputStream =  new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(menu);
            objectOutputStream.close();
            fileOutputStream.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public Set<MenuItem> DeserializeMenu()
    {
        try
        {
            Set<MenuItem> menuItems;
            FileInputStream fileInputStream = new FileInputStream("menu.ser");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            menuItems = (HashSet<MenuItem>) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
            return menuItems;
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
}
