package businessLayer;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

public class TableBLL<T> {

    private static final DateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

    public JTable createTable(Set<T> objects, Class<T> type) {
        JTable table = new JTable();
        Field[] fields = type.getDeclaredFields();
        DefaultTableModel model = new DefaultTableModel();
        Object[] columnsName = new Object[type.getDeclaredFields().length];
        int i = 0;
        for (Field currentField : fields) {
            columnsName[i] = currentField.getName();
            i++;
        }
        model.setColumnIdentifiers(columnsName);
        Object[] rowData = new Object[type.getDeclaredFields().length];
        for (T currentT : objects) {
            i = 0;
            for (Field currentField : fields) {
                currentField.setAccessible(true);
                try {
                    if (currentField.getType() == Date.class) {
                        rowData[i] = simpleDateFormat.format(currentField.get(currentT));
                    }
                    else {
                        rowData[i] = currentField.get(currentT).toString();
                    }
                    i++;
                }
                catch (IllegalAccessException e) { e.printStackTrace(); }
            }
            model.addRow(rowData);
        }
        table.setModel(model);
        return table;
    }
}
