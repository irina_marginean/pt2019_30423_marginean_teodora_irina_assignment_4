package businessLayer;

import java.util.Set;

public interface IRestaurantProcessing {
    /**
     *
     * @param menuItem to be created
     */
    void createMenuItem(MenuItem menuItem);
    /**
     *
     * @param menuItem to be edited
     * @param name to give to edited item
     * @param price to give to edited item
     * @pre getMenu().contains(menuItem)
     * @post getMenu().contains(menuItem)
     */
    void editMenuItem(MenuItem menuItem, String name, double price);
    /**
     *
     * @param menuItem to delete
     * @pre getMenu().contains(menuItem)
     * @post !getMenu().contains(menuItem)
     */
    void deleteMenuItem(MenuItem menuItem);
    /**
     *
     * @param table associated to the order
     * @param selectedItems associated to the order
     * @return the created order
     * @pre !orders.containsKey(order)
     * @post orders.containsKey(order)
     */
    Order createOrder(int table, Set<MenuItem> selectedItems);
    /**
     *
     * @param order for which the price is computed
     * @return order's price
     * @pre getAllOrders().contains(order)
     * @post price >= 0
     * @invariant isWellFormed()
     */
    double computeOrderPrice(Order order);
    /**
     *
     * @param order for which the bill is generated
     * @pre getAllOrders().contains(order)
     */
    void generateBill(Order order);
}
