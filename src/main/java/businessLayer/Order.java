package businessLayer;

import java.util.Date;
import java.util.Objects;

public class Order {
    public int orderID;
    public Date date;
    public int table;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return orderID == order.orderID &&
                table == order.table &&
                date.equals(order.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderID, date, table);
    }
}


