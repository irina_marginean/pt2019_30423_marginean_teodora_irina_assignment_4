package businessLayer;

import dataLayer.FileWriter;

import java.util.*;

public class Restaurant extends Observable implements IRestaurantProcessing  {

    private Set<MenuItem> menu;
    private static int currentMenuItemId = 0;
    private Map<Order, Set<MenuItem>> orders;
    private int currentOrderId = 0;


    public Restaurant() {
        menu = new HashSet<>();
        orders = new HashMap<>();
        currentMenuItemId = getMaximumMenuItemId() + 1;
    }

    /**
     *
     * @param menuItem to be created
     */
    public void createMenuItem(MenuItem  menuItem) {
        assert !getMenu().contains(menuItem);
        menuItem.setId(++currentMenuItemId);
        menu.add(menuItem);
        assert getMenu().contains(menuItem);
    }

    /**
     *
     * @param menuItem to be edited
     * @param name to give to edited item
     * @param price to give to edited item
     * @pre getMenu().contains(menuItem)
     * @post getMenu().contains(menuItem)
     */
    public void editMenuItem(MenuItem menuItem, String name, double price) {
        assert getMenu().contains(menuItem);
        assert price >= 0;
        menuItem.setName(name);
        menuItem.setPrice(price);
        assert getMenu().contains(menuItem);
    }

    /**
     *
     * @param menuItem to delete
     * @pre getMenu().contains(menuItem)
     * @post !getMenu().contains(menuItem)
     */
    public void deleteMenuItem(MenuItem menuItem) {
        assert getMenu().contains(menuItem);
        menu.remove(menuItem);
        assert !getMenu().contains(menuItem);
    }

    /**
     *
     * @param table associated to the order
     * @param selectedItems associated to the order
     * @return the created order
     * @pre !orders.containsKey(order)
     * @post orders.containsKey(order)
     */
    public Order createOrder(int table, Set<MenuItem> selectedItems) {
        Order order = new Order();
        order.orderID = ++currentOrderId;
        order.date = new Date();
        order.table = table;
        assert !orders.containsKey(order);
        orders.put(order, selectedItems);
        assert orders.containsKey(order);
        setChanged();
        notifyObservers();
        return order;
    }

    /**
     *
     * @param order for which the price is computed
     * @return order's price
     * @pre getAllOrders().contains(order)
     * @post price >= 0
     * @invariant isWellFormed()
     */
    public double computeOrderPrice(Order order) {
        assert getAllOrders().contains(order);
        assert isWellFormed();
        double price = 0;
        for (MenuItem menuItem : orders.get(order)) {
            price += menuItem.computePrice();
        }
        assert price >= 0;
        return price;
    }

    /**
     *
     * @param order for which the bill is generated
     * @pre getAllOrders().contains(order)
     */
    public void generateBill(Order order) {
        assert getAllOrders().contains(order);
        FileWriter fileWriter = new FileWriter();
        fileWriter.writeBill(this, order);
    }

    public Set<MenuItem> getMenu() {
        return menu;
    }

    public Map<Order, Set<MenuItem>> getOrders() {
        return orders;
    }

    public void setMenu(Set<MenuItem> menu) {
        this.menu = menu;
    }

    public Set<Order> getAllOrders()
    {
        Set<Order> allOrders = new HashSet<>();
        for (Order order : orders.keySet())
        {
            allOrders.add(order);
        }
        return  allOrders;
    }

    private boolean isWellFormed()
    {
        int counter = 0;
        Set<Order> allOrdersSet = new HashSet<>();
        for (Order order : orders.keySet())
        {
            allOrdersSet.add(order);
            counter++;
        }
        return counter == getAllOrders().size();
    }
    private int getMaximumMenuItemId()
    {
        int maxId = 0;
        for (MenuItem menuItem : menu) {
            if (menuItem.getId() > maxId) {
                maxId = menuItem.getId();
            }
        }
        return maxId;
    }
}
