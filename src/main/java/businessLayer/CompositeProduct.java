package businessLayer;

import java.util.ArrayList;
import java.util.List;

public class CompositeProduct extends MenuItem {
    private List<MenuItem> items;

    @Override
    public String toString() {

        return this.getName() + ", " + this.getPrice() + " RON";
    }

    public CompositeProduct()
    {
        items = new ArrayList<>();
    }

    public List<MenuItem> getItems() {
        return items;
    }

    public void addMenuItem(MenuItem menuItem)
    {
        items.add(menuItem);
    }

    @Override
    public double computePrice() {
        double price = 0;
        for (MenuItem menuItem : items)
        {
            price += menuItem.getPrice();
        }
        return price;
    }

    public String getItemsString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        for (MenuItem menuItem : items)
        {
            stringBuilder.append(menuItem.getName() + "\n");
        }
        return stringBuilder.toString();
    }
}
